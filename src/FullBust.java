
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * Класс основного алгоритма где состоится пересчет
 */
public class FullBust implements Runnable{
    private final Parametrs parametrs;
    private final Tasks products;
    private final JTable table;
    private final JProgressBar progressBar;
    private ArrayList<String[]> resultForTable;
    
    private ArrayList<Rezult> rezults;
    private boolean[] busts;
    private float progress;
    private float delta;
    private boolean start;
    private int[] cellRender;
    private int res;
    
    public synchronized float getProgress() {
        return progress;
    }

    public FullBust(Parametrs parametrs, Tasks products, JTable table, JProgressBar progressBar) {
    	this.progressBar = progressBar;
    	this.table = table;
        this.parametrs = parametrs;
        this.products = products;
        progress = 0;
        start = false;
        
    }

    /**
     * Запуск расчетов
     */
    public void start(){
        if(!start) {
            start = true;
            progress = 0;
            double numCombination = Math.pow(2, products.getNumProduct());
            cellRender = new int[(int) numCombination];
            delta = 1/(float) numCombination;
            busts = new boolean[products.getNumProduct()];
            rezults = new ArrayList<>((int)numCombination);
            resultForTable = new ArrayList<>((int) numCombination);
            Thread thread = new Thread(this);
            thread.start();
        }
    }

    /**
     * Пересчет числа в битовый массив
     * @param number число счетчика
     * @return true - последняя комбинация, false - еще есть комбинации
     */
    private boolean countBinary(int number) {
        int base = products.getNumProduct();
        boolean ret = true;
        for (int i = 0; i < base; i++) {
            busts[base - 1 - i] = (1 << i & number) != 0;
            if(ret){
                ret = busts[base - 1 - i];
            }
        }
        return ret;
    }
    
    /**
     * @return расчет суммы по параметрам перебора
     */
    public Rezult calc(int index) {
    	Rezult rez = new Rezult(index);
    	for(int i = 0; i < busts.length; i++)
    		if(busts[i]) {
    			if(parametrs.time + products.getProduct(i).fresh >= 1)
    				rez.setFreshFalse();
    			rez.add(products.getProduct(i));
    		}
    	return rez;
    }
    
    public void parseResult() {
    	Rezult best = rezults.get(0);
    	for(int i = 1; i < rezults.size(); i++) {
    		// ограничения
			if(rezults.get(i).getRezult().value < parametrs.max_value && rezults.get(i).getRezult().weight < parametrs.max_weight && rezults.get(i).getFresh()) {
				// проверка с предыдущим лучшим ответом
				if(best.getRezult().fresh < rezults.get(i).getRezult().fresh) {
					if((1.2 * best.getRezult().cost) < rezults.get(i).getRezult().cost) { 
						best = rezults.get(i);
					}
				}
			}else {
    			cellRender[i] = -1;
    		}
    	}
    	cellRender[best.getIndex()-1] = 1;
    	res = best.getIndex()-1;
    	System.out.println("Best products: " + best.getIndex());
    }

    /**
     * Поток расчета
     */
    @Override
    public void run() {
        int count = 0;
        boolean con = true;
        do {
            con = countBinary(count);
            count++;
            // алгоритм поиска сумм
            Rezult rez = calc(count);
            rezults.add(rez);
            resultForTable.add(rez.getStringData());
            // ========
            progress += delta;
            progressBar.setValue((int) (getProgress()*1000));
        }while(!con);
        // алгоритм отбора лучших результатов
        parseResult();
        // ==================================
        start = false;
        createTable();
    }

	private void createTable() {
		ArrayList<String[]> data = new ArrayList<>();		
		table.setModel(new TableObjectModel(resultForTable));
		table.setDefaultRenderer(Object.class, new CellRender(cellRender));
		progressBar.setValue((int) (getProgress()*1000) +1);
	}
}
