/**
 * Класс объекта над которым будут производится вычисления
 */
public class Product {

    // параметры
    float weight;
    float value;
    float cost;
    float fresh;

    //счетчик параметра
    private int count;

    public Product() {
        count = 0;
    }

    /**
     * Добавление параметра поочередно
     * @param value значение параметра
     * @return false - все значения заполнены
     */
    public boolean addValue(float value) {
        switch (count) {
            case 0:
                this.weight = value;
                break;
            case 1:
                this.value = value;
                break;
            case 2:
                this.cost = value;
                break;
            case 3:
                this.fresh = value * 0.5f;
                break;
            default:
                return false;
        }
        count++;
        return true;
    }

    @Override
    public String toString() {
        return "\tweight:\t" + weight + "\n\tvalue\t" + value + "\n\tcost:\t" + cost + "\n\tfresh:\t" + fresh;
    }
}
