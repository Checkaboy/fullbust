
import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CellRender extends DefaultTableCellRenderer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int[] cellRenderMask;
	
	public CellRender(int[] cellRender) {
		this.cellRenderMask = cellRender;
	}
	
	public void setMask (int[] mask) {
		cellRenderMask = mask;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
        if(cellRenderMask != null) {
        	if (cellRenderMask[row] == -1) {
    			c.setBackground(Color.RED);
    		} else if(cellRenderMask[row] == 1){
    			c.setBackground(Color.GREEN);
    		} else {
    			c.setBackground(row % 2 == 0 ? Color.WHITE : Color.LIGHT_GRAY);
    		}
        }
        
        return c;
	}

}