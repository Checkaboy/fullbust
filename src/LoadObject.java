import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.JSpinner;
import javax.swing.BoxLayout;
import java.awt.Container;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.SwingConstants;

public class LoadObject extends JDialog {

	private final JPanel contentPanel = new JPanel();
	public boolean isOk = false;
	private JSpinner spinner, spinner_1, spinner_2, spinner_3, spinner_4, spinner_5, spinner_7;
	
    /**
     * @return Количество создаваемых товаров
     */
    public int getNumObject(){
        return (int) spinner.getValue();
    }

    /**
     * @return Диапазон массы
     */
    public Matrix getRangeWeight(){
        return new Matrix((int) spinner_2.getValue(), (int) spinner_4.getValue());
    }

    /**
     * @return Диапазон объема
     */
    public Matrix getRangeVolume(){
        return new Matrix((int) spinner_1.getValue(), (int) spinner_3.getValue());
    }

    /**
     * @return Диапазон стоимости
     */
    public Matrix getRangeCost(){
        return new Matrix((int) spinner_5.getValue(), (int) spinner_7.getValue());
    }

    /**
     * @return Диапазон свежести
     */
    public Matrix getRangeFresh(){
        return new Matrix(0, 1);
    }
	
	/**
	 * Create the dialog.
	 */
	public LoadObject(JFrame parent) {
		setBounds(parent.getX() + parent.getWidth()/2 - 360, parent.getY() + parent.getHeight()/2 - 240, 720, 480);
		getContentPane().setLayout(new BorderLayout());
		setModal(true);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(Color.LIGHT_GRAY));
		contentPanel.add(panel);
		
		JLabel lblNewLabel = new JLabel("Количество товара на складе");
		
		spinner = new JSpinner();
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(lblNewLabel)
					.addGap(33)
					.addComponent(spinner, GroupLayout.PREFERRED_SIZE, 461, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
						.addComponent(spinner, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(Color.LIGHT_GRAY));
		contentPanel.add(panel_1);
		
		JLabel lblNewLabel_1 = new JLabel("Диапазон случайного объема");
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));
		
		JPanel panel_3 = new JPanel();
		panel_2.add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JLabel label = new JLabel("от");
		panel_3.add(label, BorderLayout.WEST);
		
		spinner_1 = new JSpinner();
		panel_3.add(spinner_1, BorderLayout.CENTER);
		
		JPanel panel_4 = new JPanel();
		panel_2.add(panel_4);
		panel_4.setLayout(new BorderLayout(0, 0));
		
		JLabel label_2 = new JLabel("до");
		panel_4.add(label_2, BorderLayout.WEST);
		
		spinner_3 = new JSpinner();
		panel_4.add(spinner_3, BorderLayout.CENTER);
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addComponent(lblNewLabel_1)
					.addGap(41)
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 457, GroupLayout.PREFERRED_SIZE)
					.addGap(21))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new LineBorder(Color.LIGHT_GRAY));
		contentPanel.add(panel_5);
		
		JLabel lblNewLabel_2 = new JLabel("Диапазон случайного веса");
		
		JPanel panel_6 = new JPanel();
		GroupLayout gl_panel_5 = new GroupLayout(panel_5);
		gl_panel_5.setHorizontalGroup(
			gl_panel_5.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_5.createSequentialGroup()
					.addComponent(lblNewLabel_2)
					.addPreferredGap(ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
					.addComponent(panel_6, GroupLayout.PREFERRED_SIZE, 455, GroupLayout.PREFERRED_SIZE))
		);
		gl_panel_5.setVerticalGroup(
			gl_panel_5.createParallelGroup(Alignment.LEADING)
				.addComponent(lblNewLabel_2, GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
				.addComponent(panel_6, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
		);
		panel_6.setLayout(new BoxLayout(panel_6, BoxLayout.X_AXIS));
		
		JPanel panel_10 = new JPanel();
		panel_6.add(panel_10);
		panel_10.setLayout(new BorderLayout(0, 0));
		
		JLabel label_3 = new JLabel("от");
		panel_10.add(label_3, BorderLayout.WEST);
		
		spinner_2 = new JSpinner();
		panel_10.add(spinner_2, BorderLayout.CENTER);
		
		JPanel panel_11 = new JPanel();
		panel_6.add(panel_11);
		panel_11.setLayout(new BorderLayout(0, 0));
		
		JLabel label_4 = new JLabel("до");
		panel_11.add(label_4, BorderLayout.WEST);
		
		spinner_4 = new JSpinner();
		panel_11.add(spinner_4, BorderLayout.CENTER);
		panel_5.setLayout(gl_panel_5);
		
		JPanel panel_12 = new JPanel();
		panel_12.setBorder(new LineBorder(Color.LIGHT_GRAY));
		contentPanel.add(panel_12);
		
		JLabel lblNewLabel_3 = new JLabel("Диапазон случайной стоимости");
		
		JPanel panel_14 = new JPanel();
		GroupLayout gl_panel_12 = new GroupLayout(panel_12);
		gl_panel_12.setHorizontalGroup(
			gl_panel_12.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_12.createSequentialGroup()
					.addComponent(lblNewLabel_3)
					.addPreferredGap(ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
					.addComponent(panel_14, GroupLayout.PREFERRED_SIZE, 455, GroupLayout.PREFERRED_SIZE))
		);
		gl_panel_12.setVerticalGroup(
			gl_panel_12.createParallelGroup(Alignment.LEADING)
				.addComponent(lblNewLabel_3, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
				.addComponent(panel_14, GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
		);
		panel_14.setLayout(new BoxLayout(panel_14, BoxLayout.X_AXIS));
		
		JPanel panel_16 = new JPanel();
		panel_14.add(panel_16);
		panel_16.setLayout(new BorderLayout(0, 0));
		
		JLabel label_5 = new JLabel("от");
		panel_16.add(label_5, BorderLayout.WEST);
		
		spinner_5 = new JSpinner();
		panel_16.add(spinner_5, BorderLayout.CENTER);
		
		JPanel panel_17 = new JPanel();
		panel_14.add(panel_17);
		panel_17.setLayout(new BorderLayout(0, 0));
		
		JLabel label_7 = new JLabel("до");
		panel_17.add(label_7, BorderLayout.WEST);
		
		spinner_7 = new JSpinner();
		panel_17.add(spinner_7, BorderLayout.CENTER);
		panel_12.setLayout(gl_panel_12);
		
		JPanel panel_13 = new JPanel();
		panel_13.setBorder(new LineBorder(Color.LIGHT_GRAY));
		contentPanel.add(panel_13);
		
		JLabel lblNewLabel_4 = new JLabel("Диапазон случайной свежести");
		
		JPanel panel_15 = new JPanel();
		GroupLayout gl_panel_13 = new GroupLayout(panel_13);
		gl_panel_13.setHorizontalGroup(
			gl_panel_13.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_13.createSequentialGroup()
					.addComponent(lblNewLabel_4)
					.addPreferredGap(ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
					.addComponent(panel_15, GroupLayout.PREFERRED_SIZE, 455, GroupLayout.PREFERRED_SIZE))
		);
		gl_panel_13.setVerticalGroup(
			gl_panel_13.createParallelGroup(Alignment.LEADING)
				.addComponent(lblNewLabel_4, GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
				.addComponent(panel_15, GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
		);
		panel_15.setLayout(new BoxLayout(panel_15, BoxLayout.X_AXIS));
		
		JPanel panel_18 = new JPanel();
		panel_15.add(panel_18);
		panel_18.setLayout(new BorderLayout(0, 0));
		
		JLabel label_6 = new JLabel("от");
		panel_18.add(label_6, BorderLayout.WEST);
		
		JLabel label_1 = new JLabel("0");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_18.add(label_1, BorderLayout.CENTER);
		
		JPanel panel_19 = new JPanel();
		panel_15.add(panel_19);
		panel_19.setLayout(new BorderLayout(0, 0));
		
		JLabel label_8 = new JLabel("до");
		panel_19.add(label_8, BorderLayout.WEST);
		
		JLabel label_9 = new JLabel("0.5");
		label_9.setHorizontalAlignment(SwingConstants.CENTER);
		panel_19.add(label_9, BorderLayout.CENTER);
		panel_13.setLayout(gl_panel_13);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			
			JButton okButton = new JButton("OK");
			okButton.setActionCommand("OK");
			buttonPane.add(okButton);
			getRootPane().setDefaultButton(okButton);
			
			JButton cancelButton = new JButton("Cancel");
			cancelButton.setActionCommand("Cancel");
			buttonPane.add(cancelButton);
			
			okButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					onOK();
				}
			});

			cancelButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					onCancel();
				}
			});
		}
	}
	
    private void onOK() {
        isOk = true;
        dispose();
    }

    private void onCancel() {
        isOk = false;
        dispose();
    }
}
