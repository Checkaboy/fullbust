
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

public class TableObjectModel extends AbstractTableModel {
	
	private static final long serialVersionUID = 1L;
	
	private String[] columnNames = {"Имя", "Вес", "Объем", "Стоимость", "Свежесть"};
    
	private ArrayList<String[]> data = new ArrayList();
    
    public TableObjectModel(ArrayList<String[]> data) {
    	this.data = data;
    	fireTableDataChanged();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
        String[] row = data.get(rowIndex);
        return row[columnIndex];
    }
    
    public void updateValue(String[] value) {
    	data.clear();
    	int numRow = value.length;
    	for (int aNumRow = 0; aNumRow < numRow; aNumRow++) {
    		String loadDataRow = (aNumRow+1) + "," + value[aNumRow];
    		String[] loadData = loadDataRow.split(",");
            data.add(loadData);
    	}
        fireTableDataChanged();
    }
    
    public void setValueAt(Object newValue, int rowIndex, int columnIndex) {
        String[] row = data.get(rowIndex);
        row[columnIndex] = newValue.toString();
        fireTableCellUpdated(rowIndex, columnIndex);
    }
    
	public int getColumnCount() {
		return columnNames.length;
	}
	
	public int getRowCount() {
		return data.size();
	}
	
	public String getColumnName(int col) {
        return columnNames[col];
    }
		
	public void clearData() {
		data.clear();
		fireTableDataChanged();
	}
	
	public String[] getRowData(int index) {
		return data.get(index);
	}
	
    public boolean isCellEditable(int row, int col) {
    	return false;
    }
}