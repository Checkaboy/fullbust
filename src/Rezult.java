/**
 * Класс результата
 * @author taras
 */
public class Rezult {
	
	private Product rezult;
	private int index;
	private boolean fresh;
	
	public boolean getFresh() {
		return fresh;
	}
	
	public Product getRezult() {
		return rezult;
	}
	
	public Rezult(int index) {
		fresh = true;
		this.index = index;
		rezult = new Product();
	}
	
	public int getIndex() {
		return index;
	}
	
	public Rezult() {
		fresh = true;
		rezult = new Product();
	}
	
	public void setFreshFalse() {
		fresh = false;
	}
	
	public String[] getStringData(){
		String[] str = new String[5];
		str[0] = "" + index;
		str[1] = "" + rezult.weight;
		str[2] = "" + rezult.value;
		str[3] = "" + rezult.cost;
		str[4] = "" + rezult.fresh;
		return str;
	}
	
	public void add(Product product) {
		rezult.weight 	+= product.weight;
		rezult.value 	+= product.value;
		rezult.cost		+= product.cost;
		rezult.fresh	+= product.fresh;
	}
	
	public void print() {
		System.out.println("combination: " + index);
		System.out.println(rezult.toString());
	}
}
