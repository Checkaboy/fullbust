import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.BorderLayout;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class CalculateParametrs extends JDialog {
	
    private JPanel contentPane;
    
    private JButton buttonOK;
    private JButton buttonCancel;
    
    boolean isOk = false;
    private JPanel panel_1;
    private JLabel label;
    private ArrayList<Parametrs> params;
    private JComboBox comboBox;

    public Parametrs getParametrs(){
        return params.get(comboBox.getSelectedIndex());
    }

    public CalculateParametrs(MainMenu parent) {
    	params = new ArrayList<>();
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setModal(true);
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
        
        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.SOUTH);
        
        buttonOK = new JButton("Ok");
        panel.add(buttonOK);
        
        buttonCancel = new JButton("Cancel");
        panel.add(buttonCancel);
        
        getRootPane().setDefaultButton(buttonOK);
        
        panel_1 = new JPanel();
        contentPane.add(panel_1, BorderLayout.NORTH);
        
        label = new JLabel("Рейс");
        
        try {
    		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbmain?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "Checkaboy_1996");
    		Statement statement = con.createStatement();
    		ResultSet resProduct = statement.executeQuery("SELECT * FROM flight");
    		while(resProduct.next()) {
    			Parametrs parametr = new Parametrs();
    			parametr.time = resProduct.getFloat(2);pack();
    			params.add(parametr);
    		}
    		resProduct = statement.executeQuery("SELECT * FROM auto");
    		while(resProduct.next()) {
    			Parametrs parametr = params.get(resProduct.getInt(1)-1);
    			parametr.max_weight = resProduct.getFloat(2);
    			parametr.max_value = resProduct.getFloat(3);
    		}
    		con.close();
    	}catch (Exception e) {
    		System.out.println(e.toString());
		}
        
        comboBox = new JComboBox();
        for(Parametrs p : params) {
        	comboBox.addItem("Вес вейса: " + p.max_value + ", объем: " + p.max_weight + ", время: " + p.time);
        }
        
        JCheckBox checkBox = new JCheckBox("Использовать ускоренный алгоритм");
        checkBox.setHorizontalAlignment(SwingConstants.CENTER);
        GroupLayout gl_panel_1 = new GroupLayout(panel_1);
        gl_panel_1.setHorizontalGroup(
        	gl_panel_1.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_panel_1.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
        				.addComponent(checkBox, GroupLayout.DEFAULT_SIZE, 690, Short.MAX_VALUE)
        				.addGroup(gl_panel_1.createSequentialGroup()
        					.addComponent(label)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(comboBox, 0, 641, Short.MAX_VALUE)))
        			.addContainerGap())
        );
        gl_panel_1.setVerticalGroup(
        	gl_panel_1.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_panel_1.createSequentialGroup()
        			.addGap(78)
        			.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
        				.addComponent(label, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
        				.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addGap(186)
        			.addComponent(checkBox)
        			.addContainerGap(105, Short.MAX_VALUE))
        );
        panel_1.setLayout(gl_panel_1);
        setResizable(false);
        setBounds(parent.getX() + parent.getWidth()/2 - 360, parent.getY() + parent.getHeight()/2 - 240, 720, 480);
        
        
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
        
    }

    private void onOK() {
        isOk = true;
        dispose();
    }

    private void onCancel() {
        isOk = false;
        dispose();
    }
}
