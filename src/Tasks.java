import java.util.ArrayList;
import java.util.Random;
import java.sql.*;

/**
 * Класс который хранит значения для основного алгоритма.
 * Предназначен как для загрузки данных из БД, так и для
 * пересоздания этих данных и запись в БД
 */
public class Tasks {

	
    private ArrayList<Product> products;

    public int getNumProduct(){
        return products.size();
    }

    public Product getProduct(int index){
        return products.get(index);
    }

    public ArrayList<Product> getAllProduct(){
        return products;
    }
    
    public ArrayList<String[]> getDataTable(){
		ArrayList<String[]> data = new ArrayList<>();
		
		int count = 0;
		for(Product p : products) {
			count++;
			String[] str = new String[5];
			str[0] = "" + count;
			str[1] = "" + p.weight;
			str[2] = "" + p.value;
			str[3] = "" + p.cost;
			str[4] = "" + p.fresh;
			data.add(str);
		}
		
		return data;
	}

    /**
     * Генератор данных и запись их в БД
     * @param numObject количество объектов
     * @param range список диапазонов каждого парамета для генерации
     */
    public Tasks(int numObject, ArrayList<Matrix> range) {
        products = new ArrayList<>(numObject);
        for (int i = 0; i < numObject; i++) {
            Product product = new Product();
            for (Matrix m : range) {
                Random random = new Random();
                product.addValue(random.nextFloat() * (m.getY() - m.getX()) + m.getX());
            }
            products.add(product);
            System.out.println("product #" + (i+1));
            System.out.println(product.toString());
        }
    }

    /**
     * Загрузка данных из БД без пересоздания
     */
    public Tasks(){
    	products = new ArrayList<>();
        // Загрузка значений из БД
    	try {
    		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbmain?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "Checkaboy_1996");
    		System.out.println("connect");
    		Statement statement = con.createStatement();
    		ResultSet resProduct = statement.executeQuery("SELECT * FROM product");
    		while(resProduct.next()) {
    			Product product = new Product();
    			product.cost = resProduct.getFloat(2);
    			product.weight = resProduct.getFloat(3);
    			product.fresh = resProduct.getFloat(4);
    			products.add(product);
    		}
    		resProduct = statement.executeQuery("SELECT * FROM conteiners");
    		while(resProduct.next()) {
    			System.out.println("product #" + resProduct.getInt(1));
    			products.get(resProduct.getInt(1)-1).value = resProduct.getFloat(2);
    			System.out.println(products.get(resProduct.getInt(1)-1).toString());
    		}
    		
    		con.close();
    	}catch (Exception e) {
    		System.out.println(e.toString());
		}
    }
}
