import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLayeredPane;
import javax.swing.JTable;
import javax.swing.JProgressBar;

public class MainMenu extends JFrame {

	private JPanel contentPane;
	private Tasks products;
	private JTable table, table_1;
	private JProgressBar progressBar;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainMenu frame = new MainMenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	public MainMenu() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1280, 720);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setMenu();
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JLayeredPane layeredPane = new JLayeredPane();
		tabbedPane.addTab("Объекты", null, layeredPane, null);
		layeredPane.setLayout(new BorderLayout(0, 0));
		
		table = new JTable();
		JScrollPane scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		layeredPane.add(scrollPane, BorderLayout.CENTER);
		
		JLayeredPane layeredPane_1 = new JLayeredPane();
		tabbedPane.addTab("Результат", null, layeredPane_1, null);
		layeredPane_1.setLayout(new BorderLayout(0, 0));
		
		table_1 = new JTable();
		JScrollPane scrollPane_1 = new JScrollPane(table_1, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		layeredPane_1.add(scrollPane_1, BorderLayout.CENTER);
		
		progressBar = new JProgressBar();
		layeredPane_1.add(progressBar, BorderLayout.NORTH);
		progressBar.setMinimum(0);
		progressBar.setMaximum(1001);
		progressBar.setStringPainted(true);
	}
	
	/**
     * Установка вехнего меню
     */
    private void setMenu(){
        JMenuBar menuBar = new JMenuBar();

        JMenu menuFile = new JMenu("Файл");
        menuBar.add(menuFile);

        JMenuItem menuOpenTable = new JMenuItem("Открыть таблицы");
        menuOpenTable.setActionCommand("NewRequest");
        menuFile.add(menuOpenTable);
        menuOpenTable.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	products = new Tasks();
            	table.setModel(new TableObjectModel(products.getDataTable()));
            }
        });

        JMenuItem menuCreateObject = new JMenuItem("Создать объекты");
        menuCreateObject.setActionCommand("NewObject");
        menuFile.add(menuCreateObject);
        menuCreateObject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoadObject object = new LoadObject(MainMenu.this);
                object.setVisible(true);
                if(object.isOk) {
                    ArrayList<Matrix> range = new ArrayList<>();
                    range.add(object.getRangeWeight());
                    range.add(object.getRangeVolume());
                    range.add(object.getRangeCost());
                    range.add(object.getRangeFresh());
                    products = new Tasks(object.getNumObject(), range);
                    table.setModel(new TableObjectModel(products.getDataTable()));
                }
            }
        });

        JMenuItem menuCalculate = new JMenuItem("Расчет");
        menuCalculate.setActionCommand("Calculate");
        menuFile.add(menuCalculate);
        menuCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CalculateParametrs calculate = new CalculateParametrs(MainMenu.this);
                calculate.setVisible(true);
                if(calculate.isOk) {
                    FullBust caalc = new FullBust(calculate.getParametrs(), products, table_1, progressBar);
                    caalc.start();
                }
                    
            }
        });

        setJMenuBar(menuBar);
    }
}
