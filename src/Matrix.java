/**
 * Класс для хранения диапазонов генерируемых значений
 */
public class Matrix {
    public enum AddElement{
        X,
        Y,
        XY,
        XnY
    }

    private int x, y;

    public Matrix(){
        x = 0;
        y = 0;
    }

    public Matrix(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Matrix(Matrix cloneMatrix){
        this.x = cloneMatrix.getX();
        this.y = cloneMatrix.getY();
    }

    public void setXY(int x, int y){
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString(){
        return "[" + x + ", " + y + "]";
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    public void add(AddElement el, int delta){
        switch (el){
            case X:
                x += delta;
                break;
            case Y:
                y += delta;
                break;
            case XY:
                x += delta;
                y += delta;
                break;
            case XnY:
                x += delta;
                y -= delta;
                break;
        }
    }
}
